Rails.application.routes.draw do
  root 'page#index'
  get 'test', to: 'page#test'
end
