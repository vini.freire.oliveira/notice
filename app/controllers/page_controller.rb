class PageController < ApplicationController
  
  def index
  end

  def test
    if User.all.count > 0
      redirect_to root_path, flash: {success: "#{User.count} users"}
    else
      redirect_to root_path, flash: {error: "#{User.count} users"}
    end
  end

end
